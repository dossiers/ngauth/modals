# @ngauth/modals

## Overview

`NgAuth` is a suite of Angular modules
comprising a set of auth-related reusable components and services.
The library is mostly frontend/UI, and it uses AWS Cognito for serverless backend.
The `NgAuth/Modals` module provides modal/dialog components,
which are simple wrappers around the form components of @ngauth/forms.



_(Note: this library is currently in beta,_
_and the APIs may change over time.)_



## Installation

To install this library, run:

```bash
$ npm install --save @ngauth/modals @ngauth/forms @ngauth/services @ngauth/core
```


Note that the ngauth components use `@angular/material`,
and if you don't enable angular material in your app,
these components will not work properly.

For angular material setup instructions, refer to this document:

* [Getting Started with Angular Material](https://gitlab.com/angularmaterial/setup)




## Using the library

From your Angular `AppModule`:

```typescript
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

// Import the library
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';
import { NgAuthFormsModule } from '@ngauth/forms';
import { NgAuthModalsModule } from '@ngauth/modals';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    // Specify the library as an import
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    NgAuthFormsModule.forRoot(),
    NgAuthModalsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

Once the library is imported, you can use its components, directives and pipes in your Angular application:

```xml
<!-- You can now use the library component in app.component.html -->
<h1>
  {{title}}
</h1>
<ngauth-modal-user-login></ngauth-modal-user-login>
```


## License

MIT © [Harry Y](mailto:sidewaybot@gmail.com)
