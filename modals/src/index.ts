import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { RouterModule } from '@angular/router';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgAuthCoreModule } from '@ngauth/core';
import { NgAuthServicesModule } from '@ngauth/services';
import { NgAuthFormsModule } from '@ngauth/forms';

// import { NgAuthMaterialModule } from './ngauth-material.module';
// import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatIconModule,
  MatButtonModule,
  MatChipsModule,
  MatTabsModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatCardModule,
  MatDialogModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatInputModule,
  MatFormFieldModule,
  MatGridListModule,
  MatStepperModule
} from '@angular/material';

import { UserLoginModalComponent } from './modals/user-login-modal/user-login-modal.component';
import { UserLogoutModalComponent } from './modals/user-logout-modal/user-logout-modal.component';
import { NewRegistrationModalComponent } from './modals/new-registration-modal/new-registration-modal.component';
import { ResendCodeModalComponent } from './modals/resend-code-modal/resend-code-modal.component';
import { ConfirmRegistrationModalComponent } from './modals/confirm-registration-modal/confirm-registration-modal.component';
import { ChangePasswordModalComponent } from './modals/change-password-modal/change-password-modal.component';
import { ResetPasswordModalComponent } from './modals/reset-password-modal/reset-password-modal.component';

export * from './modals/user-login-modal/user-login-modal.component';
export * from './modals/user-logout-modal/user-logout-modal.component';
export * from './modals/new-registration-modal/new-registration-modal.component';
export * from './modals/resend-code-modal/resend-code-modal.component';
export * from './modals/confirm-registration-modal/confirm-registration-modal.component';
export * from './modals/change-password-modal/change-password-modal.component';
export * from './modals/reset-password-modal/reset-password-modal.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    // RouterModule,
    NgCoreCoreModule.forRoot(),
    NgAuthCoreModule.forRoot(),
    NgAuthServicesModule.forRoot(),
    NgAuthFormsModule.forRoot(),
    // NgAuthMaterialModule,
    // NoopAnimationsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatTabsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatStepperModule
  ],
  declarations: [
    UserLoginModalComponent,
    UserLogoutModalComponent,
    NewRegistrationModalComponent,
    ResendCodeModalComponent,
    ConfirmRegistrationModalComponent,
    ChangePasswordModalComponent,
    ResetPasswordModalComponent,
  ],
  exports: [
    UserLoginModalComponent,
    UserLogoutModalComponent,
    NewRegistrationModalComponent,
    ResendCodeModalComponent,
    ConfirmRegistrationModalComponent,
    ChangePasswordModalComponent,
    ResetPasswordModalComponent,
  ],
  entryComponents: [
    UserLoginModalComponent,
    UserLogoutModalComponent,
    NewRegistrationModalComponent,
    ResendCodeModalComponent,
    ConfirmRegistrationModalComponent,
    ChangePasswordModalComponent,
    ResetPasswordModalComponent,
  ],
})
export class NgAuthModalsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgAuthModalsModule,
      providers: [
      ]
    };
  }
}
