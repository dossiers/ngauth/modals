import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { ResendCodeComponent } from '@ngauth/forms';
import { ConfirmRegistrationComponent } from '@ngauth/forms';


@Component({
  selector: 'ngauth-modal-resend-code',
  templateUrl: './resend-code-modal.component.html',
  styleUrls: ['./resend-code-modal.component.css']
})
export class ResendCodeModalComponent implements OnInit {

  @ViewChild("ngAuthResendCode")
  ngAuthResendCode: ResendCodeComponent;
  @ViewChild("ngAuthConfirmRegistration")
  ngAuthConfirmRegistration: ConfirmRegistrationComponent;

  constructor(
    private matSnackBar: MatSnackBar,
    private dialogRef: MatDialogRef<ResendCodeModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {
    this.ngAuthResendCode.setCallback({
      dialogOperationDone: (action) => {
        if (action == 'cancel') {
          this.dialogRef.close();
        } else {
          // ignore...
        }
      }
    });
    this.ngAuthConfirmRegistration.setCallback({
      dialogOperationDone: (action) => {
        if (action == 'cancel') {
          this.dialogRef.close();
        } else {
          // ignore...
        }
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  onResendCodeSubmitted(cognitoResult: CognitoResult) {
    // if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);

    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Resend Code',
        { duration: 2500 });

      // TBD: Go back to the first step.
      // For now, just dismiss the dialog.
      this.dialogRef.close();
    } else {
      let email = this.ngAuthResendCode.email;
      if (email) {
        // tbd: Do this only if this.ngAuthConfirmRegistration.email is not already set ????
        this.ngAuthConfirmRegistration.email = email;
      }
      // ...
    }
  }
  onRegistrationConfirmed(cognitoResult: CognitoResult) {
    // if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);

    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Confirm Registration',
        { duration: 2500 });
    } else {
      this.matSnackBar.open("Registration confirmed",
        'New Registration',
        { duration: 2500 });
    }
    this.dialogRef.close();
  }

}
