import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResendCodeModalComponent } from './resend-code-modal.component';

describe('ResendCodeModalComponent', () => {
  let component: ResendCodeModalComponent;
  let fixture: ComponentFixture<ResendCodeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResendCodeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResendCodeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
