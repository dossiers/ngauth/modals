import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { UserLoginComponent } from '@ngauth/forms';


@Component({
  selector: 'ngauth-modal-user-login',
  templateUrl: './user-login-modal.component.html',
  styleUrls: ['./user-login-modal.component.css']
})
export class UserLoginModalComponent implements OnInit, DialogParentCallback {

  @ViewChild("ngAuthUserLogin")
  ngAuthUserLogin: UserLoginComponent;

  constructor(
    private matSnackBar: MatSnackBar,
    private dialogRef: MatDialogRef<UserLoginModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
  ) {
  }

  ngOnInit() {
    this.ngAuthUserLogin.setCallback(this);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dialogOperationDone(action: (string | null)): void {
    if (action == 'cancel') {
      this.dialogRef.close();
    } else {
      // ignore...
    }
  }

  onUserLoggedIn(cognitoResult: CognitoResult) {
    if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    // Note: JSON.stringify() in the following sometimes throws an exception:
    //       "TypeError: Converting circular structure to JSON".
    // if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);
    if (cognitoResult.result && cognitoResult.result.accessToken) {
      if(isDL()) dl.log(`jwtToken =  ${cognitoResult.result.accessToken.jwtToken}`);
      // etc.
    }

    let loggedIn = false;
    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Login',
        { duration: 2500 });
    } else {
      loggedIn = true;
      this.matSnackBar.open("Successfully authenticated",
        'Login',
        { duration: 2500 });
    }
    this.dialogRef.close(loggedIn);
  }

}
