import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatSnackBar } from '@angular/material';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { CognitoResult } from '@ngauth/core';
import { DialogParentCallback } from '@ngauth/core';
import { ConfirmRegistrationComponent } from '@ngauth/forms';


@Component({
  selector: 'ngauth-modal-confirm-registration',
  templateUrl: './confirm-registration-modal.component.html',
  styleUrls: ['./confirm-registration-modal.component.css']
})
export class ConfirmRegistrationModalComponent implements OnInit, OnDestroy, DialogParentCallback {

  @ViewChild("ngAuthConfirmRegistration")
  ngAuthConfirmRegistration: ConfirmRegistrationComponent;

  email: string;
  private sub: any;

  constructor(
    private matSnackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private dialogRef: MatDialogRef<ConfirmRegistrationModalComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['username']) {
        this.email = params['username'];
        if(dl.isLoggable()) dl.log("Params.username = " + this.email);
      } else {
        if(dl.isLoggable()) dl.log("Params.username missing.");

        // tbd:
        // Cannot continue without email...
        // matSnackBar call here throws an ExpressionChangedAfterItHasBeenCheckedError.
        // this.matSnackBar.open("Username/email missing",
        //   'Confirm Registration',
        //   { duration: 2500 });

        // tbd:
        // Either dismiss this dialog
        // or, disable the submit button.
        // ...
      }
    });

    this.ngAuthConfirmRegistration.setCallback(this);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dialogOperationDone(action: (string | null)): void {
    if (action == 'cancel') {
      this.dialogRef.close();
    } else {
      // ignore...
    }
  }

  onRegistrationConfirmed(cognitoResult: CognitoResult) {
    // if(isDL()) dl.log(`cognitoResult =  ${cognitoResult.toString()}`);  // ???
    if(isDL()) dl.log(`cognitoResult =  ${JSON.stringify(cognitoResult)}`);

    if (cognitoResult.message) {
      this.matSnackBar.open(cognitoResult.message,
        'Confirm Registration',
        { duration: 2500 });
    } else {
      this.matSnackBar.open("Successfully registered",
        'Confirm Registration',
        { duration: 2500 });
    }
    this.dialogRef.close();
  }

}
