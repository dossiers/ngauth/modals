import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngauth-modal-user-logout',
  templateUrl: './user-logout-modal.component.html',
  styleUrls: ['./user-logout-modal.component.css']
})
export class UserLogoutModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
